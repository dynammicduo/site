drop database if exists ingram;

create database if not exists ingram;

use ingram;

create table Usuario(
    idUsuario               int not null auto_increment,
    nombreUsuario           varchar(254),
    email                   varchar(254),
    contrasenia             varchar(255),
    genero                  tinyint,
    fechaNacimiento         date,
    esAdministrador         boolean not null default 0,
    -- ubicacion
    idCiudad                int,
    idEstado                int,
    idPais                  int,
    -- perfil 
    fotoPerfil              blob,
    fotoPortada             blob,
    perfilPrivado           boolean,
    PRIMARY KEY (idUsuario)
);

-- Estas son las preguntas pre-definidas
create table PreguntaSeguridad(
    idPregunta              int not null auto_increment,
    pregunta                varchar(50),
    PRIMARY KEY (idPregunta)
);

-- Cada de una de las preguntas de seguridad del usuario
create table Seguridad(
    idSeguridad             int not null auto_increment,
    idPregunta              int not null,
    idUsuario               int not null,
    respuesta               varchar(50),
    FOREIGN KEY (idUsuario)         REFERENCES Usuario(idUsuario),
    FOREIGN KEY (idPregunta)        REFERENCES PreguntaSeguridad(idPregunta),
    PRIMARY KEY (idSeguridad)
);

-- Seguir a otro usuario
create table Solicitud(
    idUsuario1              int not null,
    idUsuario2              int not null,
    estadoSolicitud         tinyint,
    accionUsuario           tinyint,    
    FOREIGN KEY (idUsuario1)        REFERENCES Usuario(idUsuario),
    FOREIGN KEY (idUsuario2)        REFERENCES Usuario(idUsuario),
    PRIMARY KEY (idUsuario1, idUsuario2)
);

create table Razon(
    idRazon                 int not null auto_increment,
    textoRazon              varchar(200),
    PRIMARY KEY (idRazon)
);

create table Suspencion(
    idSuspencion            int not null auto_increment,
    idUsuario1              int not null,
    idUsuario2              int not null,
    idRazon                 int not null,
    comentario              varchar(200),
    inicioBloqueo           date,
    finBloqueo              date,
    FOREIGN KEY (idUsuario1)     	REFERENCES Usuario(idUsuario),
    FOREIGN KEY (idUsuario2)     	REFERENCES Usuario(idUsuario),
    FOREIGN KEY (idRazon)	        REFERENCES Razon(idRazon),
    PRIMARY KEY (idSuspencion)
);

create table Publicacion(
    idPublicacion           int not null auto_increment,
    idUsuario               int not null,
    textoPublicacion        varchar(141),
    tipoPublicacion         tinyint,
    imagen                  blob,
    urlVideo                varchar(2000),
    FOREIGN KEY (idUsuario)         REFERENCES Usuario(idUsuario),
    PRIMARY KEY (idPublicacion)
);

create table Reporte(
    idUsuario               int not null,
    idPublicacion           int not null,
    comentario              varchar(200),
    FOREIGN KEY (idUsuario)	        REFERENCES Usuario(idUsuario),
    FOREIGN KEY (idPublicacion)	    REFERENCES Publicacion(idPublicacion),
    PRIMARY KEY (idUsuario, idPublicacion)
);

create table MeGusta(
    idMeGusta               int not null auto_increment,
    idUsuario               int not null,
    idPublicacion           int not null,
    FOREIGN KEY (idUsuario)	        REFERENCES Usuario(idUsuario),
    FOREIGN KEY (idPublicacion)	    REFERENCES Publicacion(idPublicacion),
    PRIMARY KEY (idMeGusta)
);

create table Comentario(
    idComentario            int not null auto_increment,
    idUsuario               int not null,
    idPublicacion           int not null,
    Comentario              varchar(50),
    FOREIGN KEY (idUsuario)	        REFERENCES Usuario(idUsuario),
    FOREIGN KEY (idPublicacion)	    REFERENCES Publicacion(idPublicacion),
    PRIMARY KEY (idComentario)
);