delimiter $$
create procedure sp_Login(
	in _email 			varchar(254),
	in _contrasenia		varchar(20)
    )
begin
	select 
		idUsuario,
		nombreUsuario,
		email,
		genero,
		fechaNacimiento,
		esAdministrador,
		idCiudad,
		idEstado,
		idPais,
		fotoPerfil,
		fotoPortada
	from
		usuario
	where 
		email = _email
        and contrasenia = _contrasenia;         
end$$
delimiter ;

delimiter $$
create procedure sp_Register(
	in _nombreUsuario 	varchar(254),
	in _email 			varchar(254),
	in _contrasenia		varchar(20),
	in _genero			tinyint,
	in _fechaNacimiento date
    )
begin
	insert into usuario 
    set
		nombreUsuario = _nombreUsuario,
        email = _email,
        contrasenia = _contrasenia,
        genero = _genero,
        fechaNacimiento = _fechaNacimiento;        
end$$
delimiter ;

delimiter $$
create procedure sp_CargarFotoPortada(
	in _fotoPortada		blob,
    in _email			varchar(254),
    in _contrasenia		varchar(20)
    )
begin
	update usuario 
    set fotoPortada = _fotoPortada
	where
		email = _email
        and contrasenia = _contrasenia;
end$$
delimiter ;

delimiter $$
create procedure sp_CargarFotoPerfil(
	in _fotoPerfil		blob,
    in _email			varchar(254),
    in _contrasenia		varchar(20)
    )
begin
	update usuario 
    set fotoPerfil = _fotoPerfil
	where
		email = _email
        and contrasenia = _contrasenia;
end$$
delimiter ;

delimiter $$
create procedure sp_CrearPublicacion(
	in _email 				varchar(254),
	in _contrasenia			varchar(20),
    in _imagen				blob,
    in _tipoPublicacion		tinyint,
    in _urlVideo			varchar(2000),
    in _textoPublicacion	varchar(141)
    )
begin
	insert into publicacion
		set         
        idUsuario = (select idUsuario from usuario where email = _email and contrasenia = _contrasenia),
        tipoPublicacion = _tipoPublicacion,
        imagen = _imagen,
        urlVideo = _urlVideo,
        textoPublicacion = _textoPublicacion;
end$$
delimiter ;

delimiter $$
create procedure sp_PublicacionesUsuario(
    in _idUsuario			int
)
begin
	select 
		textoPublicacion,
		tipoPublicacion,
        imagen,
        urlVideo
    from 
		publicacion p
    join 
		usuario u
    on 
		p.idUsuario = _idUsuario;    
end$$
delimiter ;
