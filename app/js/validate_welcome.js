var main = function() {
  /*$('#profileDummy').addClass('visible');
  $('#coverDummy').addClass('visible');*/

  var profileDisplayArea = document.getElementById('profileDisplayArea');
  var coverDisplayArea = document.getElementById('coverDisplayArea');

  $('#profileDisplayArea').click(function(){

    document.getElementById('fileInputProfile').click();

  });

  $('#fileInputProfile').change(function(){
    var file = fileInputProfile.files[0];
    var imageType = /image.*/;
    if(file.size > 2097152) {
      alert('sube una imagen menos pesada');
      return;
    }

    if (file.type.match(imageType)) {
        var reader = new FileReader();

        reader.onload = function (e) {
            profileDisplayArea.innerHTML = "";

            // Create a new image.
            var img = new Image();
            // Set the img src property using the data URL.
            img.src = reader.result;

            // Add the image to the page.
            profileDisplayArea.appendChild(img);
            //console.log(reader.result);
            var formData = new FormData();
            formData.append('fileProfile', file);
            formData.append('email', localStorage.email);
            formData.append('password', localStorage.password);

            localStorage.setItem('profilePic', reader.result);

            $.ajax({
                type: "POST",
                url: "./php/updateProfile.php",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                    console.log(data);
                }
            });
        }

        if (file) {
            reader.readAsDataURL(file);
        }
    } else {
        profileDisplayArea.innerHTML = "File not supported!";
    }
  });

  $('#coverDisplayArea').click(function(event){
    //event.preventDefault();
    document.getElementById('fileInputCover').click();
  });

  $('#fileInputCover').change(function(){
    var file = fileInputCover.files[0];
    var imageType = /image.*/;
    if(file.size > 2097152) {
      alert('sube una imagen menos pesada');
      return;
    }

    if (file.type.match(imageType)) {
        var reader = new FileReader();

        reader.onload = function (e) {
            profileDisplayArea.innerHTML = "";

            // Create a new image.
            var img = new Image();
            // Set the img src property using the data URL.
            img.src = reader.result;

            // Add the image to the page.
            coverDisplayArea.appendChild(img);
            //console.log(reader.result);
            var formData = new FormData();
            formData.append('fileCover', file);
            formData.append('email', localStorage.email);
            formData.append('password', localStorage.password);
            $.ajax({
                type: "POST",
                url: "./php/updateCover.php",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                    console.log(data);
                }
            });
        }

        if (file) {
            reader.readAsDataURL(file);
        }
    } else {
        coverDisplayArea.innerHTML = "File not supported!";
    }
  });



    var userInDisplay = localStorage.username;
    var usernameProfile = document.getElementById('username');
    usernameProfile.innerHTML = userInDisplay;
    var genderInDisplay = Number(localStorage.gender);
    var genderString = "";
    if(genderInDisplay == 1) genderString = 'Bienvenido';
    else genderString = 'Bienvenida';
    document.getElementById('welcome-message').innerHTML = genderString + " a ingram, " + userInDisplay;
        $("#save").click(function() {

             var date = document.getElementsByName("birthday")[0].value;
                var today = new Date;
                var file = fileInputProfile.files[0];
                var country = $('select[name="country"]').val();
                var state = $('select[name="state"]').val();
                var city = $('select[name="city"]').val();
                if(date != "" && country != "" && state != "" && city != "") {
                    date = new Date(date);
                    date.setDate(date.getDate()+1);
                    if(date<today) {
                      var formData = new FormData();
                      formData.append('country', country);
                      formData.append('state', state);
                      formData.append('city', city);
                      formData.append('fileProfile', fileInputProfile.files[0]);
                      formData.append('fileCover', fileInputCover.files[0]);
                      formData.append('email', localStorage.username);
                      formData.append('password', localStorage.password);
                      $.ajax({
                          type: "POST",
                          url: "./php/updateProfile.php",
                          data: formData,
                          processData: false,
                          contentType: false,
                          success: function(data){
                              console.log(data);
                          }
                      });
                        }
                        else {
                            alert("fecha no valida");
                        }
                }
                else {
                    alert("llena los campos");
                }

        });


    }

$(document).ready(main);
