var main = function() {
  var usernameProfile = document.getElementById('username');
  usernameProfile.innerHTML = localStorage.username;

  var imgSrc = "";

  $('.btn').click(function() {
    var file = imagePost.files[0];
    var textPost = $('.status-box').val();

    var post = [
          '<p><img src="',
          localStorage.profilePic,
          '" width="64" />',
          '<strong>' +
          localStorage.username +
          '</strong>',
          '</p>',
          '<p>',
          textPost,
          '</p>',
          '<li><img src="' +
          imgSrc +
          '" alt="Foto de contacto" />',
          '</li>'
        ]

    console.log(localStorage.email);
    console.log(localStorage.password);

    var urlVideoPost = $("#urlVideoPost").val();
    var postTypeNum = 0;

    var formData = new FormData();
    formData.append('image', imagePost.files[0]);
    formData.append('email', localStorage.email);
    formData.append('password', localStorage.password);
    formData.append('urlVideo', urlVideoPost);
    formData.append('postType', postTypeNum);
    formData.append('postText', textPost);

    if(urlVideoPost != ""){
      formData.append('image', "");
      formData.append('postType', 1);
    }

    $.ajax({
        type: "POST",
        url: "./php/createPost.php",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            console.log(data);
        }
    });

    $(post.join("")).prependTo('.posts');
    $('.status-box').val('');
    $('.counter').text('140');
    $('.btn').addClass('disabled');
  });

  $('.status-box').keyup(function() {
    var postLength = $(this).val().length;
    var charactersLeft = 140 - postLength;
    $('.counter').text(charactersLeft);

    if(charactersLeft < 0) {
      $('.btn').addClass('disabled');
      // si quieres quitar el alert
      alert("Ha excedido el número máximo de caracteres");
    }
    else if(charactersLeft == 140) {
      $('.btn').addClass('disabled');
    }
    else {
      $('.btn').removeClass('disabled');
    }
  });

  $('.btn').addClass('disabled');

  $('#imagePost').change(function(){
      var file = imagePost.files[0];

      if(file.size > 2097152) {
        alert('sube una imagen menos pesada o con formato correcto');
        return;
      }

    var reader = new FileReader();

      reader.onload = function () {
        imgSrc = reader.result;

      }

      if (file) {
          reader.readAsDataURL(file);
      }
    });
}

$(document).ready(main);
