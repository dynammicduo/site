var main = function() {
  
  $('.general').addClass('selected');

  $('.edit-general').addClass('visible');

  $('.edit-password').addClass('not-visible');

  $('.password').click(function() {
    $('.edit-general').addClass('not-visible');
    $('.edit-general').removeClass('visible');
    $('.edit-password').addClass('visible');
    $('.edit-password').removeClass('not-visible');
    $('.general').removeClass('selected');
    $('.password').addClass('selected');
  });

  $('.general').click(function() {
    $('.edit-general').removeClass('not-visible');
    $('.edit-general').addClass('visible');
    $('.edit-password').removeClass('visible');
    $('.edit-password').addClass('not-visible');
    $('.general').addClass('selected');
    $('.password').removeClass('selected');
  });
}

$(document).ready(main);
