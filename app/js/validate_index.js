var main = function() {
  $("#login").click(function() {
      var email = $('input:text[name="correo"]').val();
      var password = $('input:password[name="contrasenia"]').val();
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      if(email == "" || password == "") {
          alert("Introduce todos los datos de inicio de sesión");
          return;
        }
      if(regex.test(email)) {
          $.post( "./php/getUser.php",{
                    username : email,
                    password : password
            }, function( responseText ) {
                console.log(responseText);
                if(responseText === 'El correo o contraseña no son válidos') {
                    alert(responseText);
                }
                else {
                    if(IsJsonString(responseText)) {
                        var userInfo = JSON.parse(responseText);
                        localStorage.setItem('idUsuario', userInfo.idUsuario);
                        localStorage.setItem('username', userInfo.nombreUsuario);
                        localStorage.setItem('email', userInfo.email);
                        localStorage.setItem('password', password);
                        localStorage.setItem('gender', userInfo.genero);
                        localStorage.setItem('admin', userInfo.esAdministrador);
                        localStorage.setItem('birthday', userInfo.fechaNacimiento);
                        localStorage.setItem('profilePic', userInfo.fotoPerfil)
                        localStorage.setItem('coverPic', userInfo.fotoPortada);
                        localStorage.setItem('city', userInfo.idCiudad);
                        localStorage.setItem('state', userInfo.idEstado);
                        localStorage.setItem('country', userInfo.idPais);
                        console.log(userInfo);
                        //para remover localStorage.removeItem(key);
                    }
                    else {
                        alert('Ha ocurrido un error, intenta de nuevo');
                        return;
                    }
                     window.location.href = 'home.html';
                }
            });
      } else {
          alert("Ingresa un correo valido");
      }

    });

    $("#signup").click(function() {
      var data = new Object();

      var name = $('input:text[name="nombre"]').val();
      var lastName = $('input:text[name="apellido"]').val();
      data.nombreUsuario = name + " " + lastName;
      data.email = $('input:text[name="email"]').val();
      var confirmEmail = $('input:text[name="email-com"]').val();
      data.password = $('input:password[name="pass"]').val();
      var confirmPassword = $('input:password[name="pass-com"]').val();
      data.birthday = document.getElementsByName("birthday")[0].value;
      var today = new Date;

      data.genero = $('input:radio[name="genero"]:checked').val();
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      if(name != "" && lastName != "" &&
        data.email != "" && confirmEmail != "" &&
        data.password != "" && confirmPassword != "" &&
        data.birthday != "" && data.genero != undefined) {
            data.birthday = new Date(data.birthday);
            data.birthday.setDate(data.birthday.getDate()+1);
            if(regex.test(data.email)) {
                if(confirmEmail == data.email) {
                    if(confirmPassword == data.password) {
                        if(data.birthday<today) {
                            var myJsonString = JSON.stringify(data);

                            console.log(data);
                            console.log(myJsonString);

                            $.post('./php/registerUser.php', {
                                    userJSON : myJsonString
                            }, function(responseText) {
                                if(responseText === 'Hubo un error al momento de registrar el usuario') {
                                    alert(responseText);
                                }
                                else {
                                    if(IsJsonString(responseText)) {
                                        var userInfo = JSON.parse(responseText);
                                        localStorage.setItem('username', userInfo.nombreUsuario);
                                        localStorage.setItem('email', userInfo.email);
                                        localStorage.setItem('password', data.password);
                                        localStorage.setItem('gender', userInfo.genero);
                                        localStorage.setItem('admin', userInfo.esAdministrador);
                                        localStorage.setItem('birthday', userInfo.fechaNacimiento);
                                        localStorage.setItem('profilePic', userInfo.fotoPerfil)
                                        localStorage.setItem('coverPic', userInfo.fotoPortada);
                                        localStorage.setItem('city', userInfo.idCiudad);
                                        localStorage.setItem('state', userInfo.idEstado);
                                        localStorage.setItem('country', userInfo.idPais);
                                    }
                                    else {
                                        alert('Ha ocurrido un error, intenta de nuevo');
                                        return;
                                    }
                                    window.location.href = 'welcome.html';
                                }
                            });
                        }
                        else {
                            alert("fecha no valida");
                        }
                    }
                    else {
                        alert("los passwords no coinciden");
                    }
                }
                else {
                    alert("los correos no coinciden");
                }
            }
            else {
                alert("Ingresa un correo valido");
            }
      }
      else {
          alert("llena todos los campos");
      }


    });

    function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
}

$(document).ready(main);
