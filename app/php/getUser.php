<?php

require_once('connection.php');
require_once('usuario.php');

$username = $_POST['username'];
$password = $_POST['password'];

$c = new Connection();
$conn = $c->connection;

$securePassword = hash('ripemd160', $password);

$stmt = $conn->prepare("call sp_Login(:username, :password);");
$stmt->bindParam(':username', $username);
$stmt->bindParam(':password', $securePassword);
$stmt->execute();

if($rs = $stmt->fetch(PDO::FETCH_ASSOC))
{
    $img = $rs['fotoPerfil'];
    $base64 = 'data:image/png;base64,' . base64_encode($img);

    $rs['fotoPerfil'] = $base64;

    $img = $rs['fotoPortada'];
   $base64 = 'data:image/png;base64,' . base64_encode($img);

   $rs['fotoPortada'] = $base64;    

    $userJSON = json_encode($rs);
    echo $userJSON;
}
else
{
    echo 'El correo o contraseña no son válidos';
}

$c->closeConnection();
?>
