<?php

require_once('connection.php');

$username   = $_POST["email"];
$password   = $_POST["password"];
$securePassword = hash('ripemd160', $password);
$size       = $_FILES['image']['size'];
$urlVideo   = $_POST["urlVideo"];
$postType   = $_POST["postType"];
$postText   = $_POST["postText"];

$profile_file_name = $_FILES['image']['name'];
$imageFile = fopen($_FILES['image']['tmp_name'], 'r');

if($imageFile)
{
  $imageBlob = fread($imageFile, $size);
  fclose($imageFile);
} else {
  $imageBlob = NULL;
}

$c = new Connection();
$conn = $c->connection;

$stmt = $conn->prepare("call sp_CrearPublicacion(:username, :password, :img, :postType, :urlVideo, :postText);");
$stmt->bindParam(':username', $username);
$stmt->bindParam(':password', $securePassword);
$stmt->bindParam(':img', $imageBlob);
$stmt->bindParam(':postType', $postType);
$stmt->bindParam(':urlVideo', $urlVideo);
$stmt->bindParam(':postText', $postText);
$stmt->execute();

echo "OK";

$c->closeConnection();
?>
