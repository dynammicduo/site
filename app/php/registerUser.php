<?php

require_once('connection.php');
require_once('usuario.php');

function agregarUsuario($user)
{
    $agregado = false;
    $c = new Connection();
    $conn = $c->connection;

    if($conn != null)
    {

        $securePassword = hash('ripemd160', $user->password);
        $stmt = $conn->prepare("call sp_Register(:nombreUsuario, :email,
                            :password, :genero, :fechaNacimiento);");
        $stmt->bindParam(':nombreUsuario', $user->nombreUsuario);
        $stmt->bindParam(':email', $user->email);
        $stmt->bindParam(':password', $securePassword);
        $stmt->bindParam(':genero', $user->genero);
        $stmt->bindParam(':fechaNacimiento', $user->birthday);

        $stmt->execute();

        $agregado = true;
    }
    $c->closeConnection();

    return $agregado;
}

$JSONrequest = trim($_POST['userJSON']);
$data = json_decode($JSONrequest, true);
$usuario = new Usuario('', '', '', '', '');
foreach ($data as $key => $value) $usuario->{$key} = $value;

if(agregarUsuario($usuario)) {
    $userJSON = json_encode($usuario);
    echo $userJSON;
}

else
    echo 'Hubo un error al momento de registrar el usuario';

?>
