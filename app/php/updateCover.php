<?php

require_once('connection.php');

$username = $_POST["email"];
$password = $_POST["password"];
$securePassword = hash('ripemd160', $password);
$size = $_FILES['fileCover']['size'];

$profile_file_name = $_FILES['fileCover']['name'];
$imageFile = fopen($_FILES['fileCover']['tmp_name'], 'r');

if($imageFile)
{
  $imageBlob = fread($imageFile, $size);
  fclose($imageFile);
}

$c = new Connection();
$conn = $c->connection;

$stmt = $conn->prepare("call sp_CargarFotoPortada(:img, :username, :password);");
$stmt->bindParam(':username', $username);
$stmt->bindParam(':password', $securePassword);
$stmt->bindParam(':img', $imageBlob);
$stmt->execute();

echo "OK";

$c->closeConnection();
?>
