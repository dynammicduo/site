<?php

class Connection
{

    protected static $connection;

    public function __construct() 
    {
        try 
        {
            if(!isset(self::$connection)) 
            {
                $config = parse_ini_file('config.ini');
                $servername = $config['server'];
                $db = $config['dbname'];
                self::$connection = new PDO("mysql:host=$servername; dbname=$db", $config['username'], $config['password']);
                self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
        }

        catch(PDOException $e) 
        {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function __get($connection) 
    {
        return self::$connection;
    }

    public function closeConnection()
    {
        self::$connection = null;
    }
    
}

?>