<?php

require_once('connection.php');

$username = $_POST["email"];
$password = $_POST["password"];
$size = $_FILES['fileProfile']['size'];

$profile_file_name = $_FILES['fileProfile']['name'];
$imageFile = fopen($_FILES['fileProfile']['tmp_name'], 'r');

if($imageFile)
{
  $imageBlob = fread($imageFile, $size);
  fclose($imageFile);
}

$username = $_POST['email'];
$password = $_POST['password'];
$securePassword = hash('ripemd160', $password);

$c = new Connection();
$conn = $c->connection;

$stmt = $conn->prepare("call sp_CargarFotoPerfil(:img, :username, :password);");
$stmt->bindParam(':username', $username);
$stmt->bindParam(':password', $securePassword);
$stmt->bindParam(':img', $imageBlob);
$stmt->execute();

echo "OK";

$c->closeConnection();
?>
