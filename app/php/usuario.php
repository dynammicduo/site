<?php

class Usuario 
{
    public $nombreUsuario;
    public $email;
    public $password;
    public $genero;
    public $birthday;

    public function __construct($nombreUsuario, $email, $password, $genero, $birthday)
    {
        $this->nombreUsuario = $nombreUsuario;
        $this->email = $email;
        $this->password = $password;
        $this->genero = $genero;
        $this->birthday = $birthday;
    }

    public function __get($property)
    {
        if (property_exists($this, $property)) 
        {
            return $this->$property;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) 
        {
            $this->$property = $value;
        }
    }

    

    

    
}

?>